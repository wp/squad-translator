import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {SupportedLanguagesApiService} from "../../services/api/supported-languages-api.service";
import {LanguageService} from "../../services/language.service";
import {Language} from "../../models/language.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  languages!: Language[];
  selectedLanguage!: Language;

  constructor(private authService: AuthService,
              private supportedLanguagesApiService: SupportedLanguagesApiService,
              private languageService: LanguageService,
              private router: Router) {
  }

  ngOnInit() {
    this.supportedLanguagesApiService.getLanguages().subscribe({
      next: (languages: Language[]) => {
        this.languages = languages;
        this.selectedLanguage = languages[0];
        this.languageService.languages = languages;
        this.languageService.setLanguage(languages[0])
      }
    });
  }

  logout(): void {
    this.authService.logout()
  }

  goHome(): void {
    this.router.navigateByUrl('')
  }
}
