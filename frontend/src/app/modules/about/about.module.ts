import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AboutComponent} from './about.component';
import {NavbarModule} from "../navbar/navbar.module";
import {RouterModule} from "@angular/router";

const routes = [{
  path: '',
  component: AboutComponent
}]

@NgModule({
  declarations: [
    AboutComponent
  ],
  imports: [
    CommonModule,
    NavbarModule,
    RouterModule.forChild(routes)
  ]
})
export class AboutModule {
}
