import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule, Routes} from "@angular/router";
import {NavbarModule} from "../navbar/navbar.module";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {TextCorrectionComponent} from './text-correction/text-correction.component';
import {NzInputModule} from "ng-zorro-antd/input";
import {FormsModule} from "@angular/forms";
import {AnswerTextCorrectionComponent} from './answer-text-correction/answer-text-correction.component';
import {
  HighlightAnswerModalComponent
} from './answer-text-correction/highlight-answer-modal/highlight-answer-modal.component';
import {NzModalModule} from "ng-zorro-antd/modal";


const routes: Routes = [{
  path: '',
  component: HomeComponent
}]

@NgModule({
  declarations: [
    HomeComponent,
    TextCorrectionComponent,
    AnswerTextCorrectionComponent,
    HighlightAnswerModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavbarModule,
    NzSpinModule,
    NzPaginationModule,
    NzButtonModule,
    NzIconModule,
    NzInputModule,
    FormsModule,
    NzModalModule
  ]
})
export class HomeModule {
}
