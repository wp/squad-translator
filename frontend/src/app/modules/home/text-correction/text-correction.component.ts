import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TextCorrection} from "../../../models/text-correction.model";
import {ValidationApiService} from "../../../services/api/validation-api.service";
import {finalize} from "rxjs";
import {TextCorrectionApiService} from "../../../services/api/text-correction-api.service";
import {LanguageService} from "../../../services/language.service";

@Component({
  selector: 'app-text-correction',
  templateUrl: './text-correction.component.html',
  styleUrls: ['./text-correction.component.scss']
})
export class TextCorrectionComponent implements OnInit {
  @Input() textCorrections!: TextCorrection[];
  @Input() textId!: string;
  @Input() flexColumnPosition: boolean = false;

  @Output() onTextHoverChange: EventEmitter<TextCorrection | null> = new EventEmitter<TextCorrection | null>();

  validatedTextCorrection!: TextCorrection | undefined;
  isEditMode: boolean = false;
  isRequesting: boolean = false;
  text: string = "";
  editingCorrection!: TextCorrection;

  constructor(private validationApiService: ValidationApiService,
              private textCorrectionApiService: TextCorrectionApiService,
              private languageService: LanguageService) {
  }

  ngOnInit() {
    this.validatedTextCorrection = this.textCorrections.find((textCorrection: TextCorrection) => !!textCorrection.validation)
  }

  approveTranslation(correction?: TextCorrection): void {
    this.isRequesting = true;
    if (this.isEditMode) {
      this.textCorrectionApiService
        .createTextCorrection(correction?.textId || this.textId, this.text)
        .pipe(finalize(() => this.isRequesting = false))
        .subscribe({
          next: (textCorrection: TextCorrection) => this.textCorrections.push(textCorrection)
        })
      this.isEditMode = false;
    } else {
      if (correction) {
        this.validationApiService.validateText(correction.id)
          .pipe(finalize(() => this.isRequesting = false))
          .subscribe({
            next: (updatedCorrection: TextCorrection) => {
              this.validatedTextCorrection = updatedCorrection;
              this.textCorrections.map((correction: TextCorrection) => correction.validation = null)
              const existCorrectionIdx = this.textCorrections
                .findIndex((textCorrection: TextCorrection) => textCorrection.id === updatedCorrection.id)

              if (existCorrectionIdx > -1) {
                this.textCorrections[existCorrectionIdx] = updatedCorrection;
              }
            }
          })
      }
    }
  }

  editTranslation(correction: TextCorrection): void {
    this.text = correction.text;
    this.editingCorrection = correction
    this.isEditMode = true;
  }

  editValidatedCorrection(): void {
    this.validatedTextCorrection = undefined;
  }
}
