import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Paragraph} from "../../models/paragraph.model";
import {ParagraphService} from "../../services/paragraph.service";
import {finalize} from "rxjs";
import {Answer} from "../../models/answer.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  totalParagraphs: number = 0
  selectedParagraph!: Paragraph | null;
  isFetchingParagraph = true;
  currentPage: number = 1;

  @ViewChild('paragraphText') paragraphText!: ElementRef;

  constructor(private paragraphService: ParagraphService) {
  }

  ngOnInit(): void {
    this.fetchParagraph();
  }

  fetchParagraph(): void {
    this.isFetchingParagraph = true;
    this.paragraphService.getParagraphByPage(this.currentPage - 1)
      .pipe(finalize(() => this.isFetchingParagraph = false))
      .subscribe({
        next: ((paragraph: Paragraph) => {
          this.selectedParagraph = paragraph;
          this.totalParagraphs = this.paragraphService.getTotalParagraphs();
        })
      })
  }

  onPageIndexChange(page: number) {
    this.selectedParagraph = null;
    this.currentPage = page;
    this.fetchParagraph();
  }

  onAnswerHover(answer: Answer) {
    const startHTML = "<span class='highlight-answer'>"
    const endHTML = "</span>"
    const text = this.paragraphText.nativeElement.innerText;

    const textWithClass = text.substring(0, answer.startFrom) + startHTML + text.substring(answer.startFrom);
    const startAtWithClass = answer.startFrom + startHTML.length + answer.text.length;

    this.paragraphText.nativeElement.innerHTML =
      textWithClass.substring(0, startAtWithClass) + endHTML + textWithClass.substring(startAtWithClass);
  }

  onAnswerHoverOut() {
    this.paragraphText.nativeElement.innerHTML = this.selectedParagraph?.text;
  }
}
