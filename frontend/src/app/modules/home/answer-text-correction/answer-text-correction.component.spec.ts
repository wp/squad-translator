import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerTextCorrectionComponent } from './answer-text-correction.component';

describe('AnswerTextCorrectionComponent', () => {
  let component: AnswerTextCorrectionComponent;
  let fixture: ComponentFixture<AnswerTextCorrectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnswerTextCorrectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerTextCorrectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
