import {Component, Input, OnInit} from '@angular/core';
import {TextCorrection} from "../../../models/text-correction.model";
import {Question} from "../../../models/question.model";
import {Paragraph} from "../../../models/paragraph.model";
import {NzModalService} from "ng-zorro-antd/modal";
import {HighlightAnswerModalComponent} from "./highlight-answer-modal/highlight-answer-modal.component";
import {TextCorrectionApiService} from "../../../services/api/text-correction-api.service";
import {finalize} from "rxjs";
import {ValidationApiService} from "../../../services/api/validation-api.service";

@Component({
  selector: 'app-answer-text-correction',
  templateUrl: './answer-text-correction.component.html',
  styleUrls: ['./answer-text-correction.component.scss']
})
export class AnswerTextCorrectionComponent implements OnInit {
  @Input() question!: Question;
  @Input() selectedParagraph!: Paragraph;

  validatedTextCorrection!: TextCorrection | undefined;
  isRequesting: boolean = false;

  constructor(private nzModalService: NzModalService, private textCorrectionApiService: TextCorrectionApiService, private validationApiService: ValidationApiService) {
  }

  ngOnInit(): void {
    this.validatedTextCorrection = this.question.answers[0].textCorrections.find((textCorrection: TextCorrection) => !!textCorrection.validation)
  }

  onTranslatedAnswerHoverChange(textCorrection: TextCorrection) {
    const startHTML = "<span class='highlight-answer'>"
    const endHTML = "</span>"
    const translatedParagraphEl = document.getElementById(`p:${this.selectedParagraph.id}`)
    const paragraphText = translatedParagraphEl?.innerText

    if (paragraphText && textCorrection) {
      const answerStartIdx = paragraphText.search(textCorrection.text);

      if (answerStartIdx > -1) {
        const textWithClass = paragraphText.substring(0, answerStartIdx) + startHTML + paragraphText.substring(answerStartIdx);
        const startAtWithClass = answerStartIdx + startHTML.length + textCorrection?.text.length;

        translatedParagraphEl.innerHTML =
          textWithClass.substring(0, startAtWithClass) + endHTML + textWithClass.substring(startAtWithClass);
      }
    }
  }

  onTranslatedAnswerHoverOut() {
    const translatedParagraphEl = document.getElementById(`p:${this.selectedParagraph.id}`)
    const paragraphText = translatedParagraphEl?.innerText

    if (translatedParagraphEl && paragraphText) {
      translatedParagraphEl.innerHTML = paragraphText;
    }
  }

  editValidatedCorrection(): void {
    this.validatedTextCorrection = undefined;
  }

  filterAnswerTextCorrections(): TextCorrection[] {
    const validatedParagraph = this.selectedParagraph.textCorrections.find((textCorrection: TextCorrection) => !!textCorrection.validation)
    const textToCheck = validatedParagraph?.text || this.selectedParagraph.textCorrections[0].text || ''

    return this.question.answers[0].textCorrections.filter((textCorrection: TextCorrection) => textToCheck.search(textCorrection.text) > -1)
  }

  approveTranslation(correction: TextCorrection): void {
    this.isRequesting = true;

    this.validationApiService.validateText(correction.id)
      .pipe(finalize(() => this.isRequesting = false))
      .subscribe({
        next: (updatedCorrection: TextCorrection) => {
          this.validatedTextCorrection = updatedCorrection;
        }
      })
  }

  openAddAnswerModal(): void {
    const validatedParagraph = this.selectedParagraph.textCorrections.find((textCorrection: TextCorrection) => !!textCorrection.validation)

    this.nzModalService.create({
      nzContent: HighlightAnswerModalComponent,
      nzComponentParams: {
        paragraphText: validatedParagraph?.text || this.selectedParagraph.textCorrections[0].text
      },
      nzOkText: 'Save',
      nzOnOk: (highlightAnswer: HighlightAnswerModalComponent) => {
        this.isRequesting = true;
        this.textCorrectionApiService
          .createTextCorrection(`a:${this.question.answers[0].id}`, highlightAnswer.selectedAnswer)
          .pipe(finalize(() => this.isRequesting = false))
          .subscribe({
            next: (textCorrection: TextCorrection) => this.question.answers[0].textCorrections.push(textCorrection)
          })
      }
    })
  }
}
