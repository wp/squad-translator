import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-highlight-answer-modal',
  templateUrl: './highlight-answer-modal.component.html',
  styleUrls: ['./highlight-answer-modal.component.scss']
})
export class HighlightAnswerModalComponent {
  @Input() paragraphText!: string;
  isSelectingAnswer: boolean = false;
  selectedAnswer: string = '';

  selectAnswer(ev: any): void {
    if (ev.type === 'click' && ev.shiftKey) {
      this.selectedAnswer = document.getSelection()?.toString() || '';
    } else if (ev.type === 'dblclick') {
      this.selectedAnswer = document.getSelection()?.toString() || '';
    } else if (this.isSelectingAnswer) {
      this.selectedAnswer = document.getSelection()?.toString() || '';
    }
  }
}
