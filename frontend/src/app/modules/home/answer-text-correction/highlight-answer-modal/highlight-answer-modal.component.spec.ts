import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighlightAnswerModalComponent } from './highlight-answer-modal.component';

describe('HighlightAnswerModalComponent', () => {
  let component: HighlightAnswerModalComponent;
  let fixture: ComponentFixture<HighlightAnswerModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HighlightAnswerModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlightAnswerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
