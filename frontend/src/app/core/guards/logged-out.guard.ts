import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import {AuthService} from "../../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class LoggedOutGuard implements CanActivate, CanLoad {

  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
    return this.canLoadOrActivate();
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree {
    return this.canLoadOrActivate();
  }

  private canLoadOrActivate(): boolean | UrlTree {
    if (this.authService.getStoredTokens()) {
      return this.router.createUrlTree(['/home']);
    }

    return true;
  }
}
