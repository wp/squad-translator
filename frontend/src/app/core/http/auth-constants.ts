export const InterceptorSkipAuthHeader = 'skip-auth-header-interceptor';

export const AUTH_ERROR_CODES: any = {
  invalidRefreshToken: 'REFRESH_TOKEN_INVALID'
};

export const USER_STORAGE_KEYS: any = {
  user: 'user'
};

export const AUTH_HEADER_KEYS: any = {
  apiToken: 'Authorization'
};
