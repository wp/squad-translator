import {Inject, Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {catchError, filter, first, Observable, Subject, switchMap, throwError} from 'rxjs';
import {BASE_API_URL} from '../../app.module';
import {AuthService} from "../../services/auth.service";
import {AUTH_HEADER_KEYS, InterceptorSkipAuthHeader} from "./auth-constants";
import {FORBIDDEN, UNAUTHORIZED} from "./http-codes";

@Injectable()
export class SquadHttpInterceptor implements HttpInterceptor {
  refreshingTokenInProgress = false;
  apiToken$: Subject<{ token: string | null, error: any }> = new Subject<{ token: string | null, error: any }>();

  constructor(@Inject(BASE_API_URL) private baseApiUrl: string, private authService: AuthService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url = `${this.baseApiUrl}/${request.url}`;
    const requestWithFullUrl: HttpRequest<any> = request.clone({url});
    const apiToken: string | undefined = this.authService.getStoredTokens()?.accessToken;

    let requestToHandle: HttpRequest<any>;
    if (this.shouldNotIntercept(request)) {
      requestToHandle = request.clone({
        url,
        headers: request.headers.delete(InterceptorSkipAuthHeader)
      });
    } else {
      requestToHandle = this.addApiTokenToHeaders(requestWithFullUrl, apiToken || '');
    }


    return next.handle(requestToHandle)
      .pipe(filter((event: any) => event instanceof HttpResponse),
        catchError((error: HttpErrorResponse) => {
          if (apiToken == null) {
            return throwError(() => error);
          } else if (error.status === UNAUTHORIZED || error.status === FORBIDDEN) {
            if (!this.authService.isRefreshTokenExpired()) {
              return this.refreshToken(requestWithFullUrl, next)
                .pipe(catchError(() => {
                  console.log("Refreshing token failed, will clear storage and re-throw the error");
                  this.authService.logout();
                  return throwError(() => error);
                }));
            }

            this.authService.logout();
          }

          return throwError(() => error);
        })
      )
  }

  private shouldNotIntercept(request: HttpRequest<any>): boolean {
    return request.headers.has(InterceptorSkipAuthHeader);
  }

  private addApiTokenToHeaders(request: HttpRequest<any>, apiToken: string | null): HttpRequest<any> {
    const headers: HttpHeaders = request.headers.set(AUTH_HEADER_KEYS.apiToken, `Bearer ${apiToken}`);
    return request.clone({headers});
  }

  private refreshToken(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.refreshingTokenInProgress) {
      this.refreshingTokenInProgress = true;

      console.log('Going to attempt to refresh token...');
      return this.authService.refreshToken().pipe(
        switchMap((response: string) => {
          this.refreshingTokenInProgress = false;
          this.apiToken$.next({token: response, error: null});
          return next.handle(this.addApiTokenToHeaders(request, response));
        }),
        catchError((error: any) => {
          console.log('Refresh token request failed');
          this.refreshingTokenInProgress = false;
          const refreshTokenError: any = {error, logout: error.logout || false};
          this.apiToken$.next({token: null, error: refreshTokenError});
          return throwError(() => refreshTokenError);
        })
      );
    } else {
      // if another request is refreshing the token, wait for the new token
      return this.apiToken$.pipe(
        first(),
        switchMap((apiToken: { token: string | null, error: any }) => {
          if (apiToken.error) {
            // we cannot continue without a valid refresh token... no need to re-emit logout,
            // as the original refresh token did that already.
            return throwError(() => apiToken.error.logout ? apiToken.error.error : apiToken.error);
          }
          return next.handle(this.addApiTokenToHeaders(request, apiToken.token));
        }));
    }
  }
}
