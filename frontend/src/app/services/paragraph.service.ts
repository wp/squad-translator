import {Injectable} from '@angular/core';
import {Paragraph} from "../models/paragraph.model";
import {map, Observable, tap} from "rxjs";
import {ParagraphApiService} from "./api/paragraph-api.service";
import {PageApi} from "../models/page-api.model";

@Injectable({
  providedIn: 'root'
})
export class ParagraphService {
  private totalParagraphs: number = 0

  constructor(private paragraphApiService: ParagraphApiService) {
  }

  getParagraphByPage(page: number): Observable<Paragraph> {
    return this.paragraphApiService.getParagraphByPage(page)
      .pipe(tap((response: PageApi<Paragraph>) => {
          this.totalParagraphs = response.totalElements
        }),
        map((response: PageApi<Paragraph>) => response.content[0]))
  }

  getTotalParagraphs(): number {
    return this.totalParagraphs
  }
}
