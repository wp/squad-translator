import {Injectable} from '@angular/core';
import {Language} from "../models/language.model";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  private _languages!: Language[];
  private selectedLanguage: BehaviorSubject<Language | null> = new BehaviorSubject<Language | null>(null);
  selectedLanguage$: Observable<Language | null> = this.selectedLanguage.asObservable();

  constructor() {
  }

  get languages(): Language[] {
    return this._languages;
  }

  set languages(value: Language[]) {
    this._languages = value;
  }

  setLanguage(language: Language) {
    this.selectedLanguage.next(language)
  }

  getSelectedLanguage(): Language | null {
    return this.selectedLanguage.value;
  }
}
