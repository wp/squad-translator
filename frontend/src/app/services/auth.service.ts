import {Injectable} from '@angular/core';
import {map, Observable, tap} from "rxjs";
import {User} from "../models/user.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {InterceptorSkipAuthHeader, USER_STORAGE_KEYS} from "../core/http/auth-constants";
import {Tokens} from "../models/tokens.model";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {
  }

  login(username: string, password: string): Observable<User> {
    const params: URLSearchParams = new URLSearchParams();
    params.set('username', username);
    params.set('password', password);

    const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
      .set(InterceptorSkipAuthHeader, InterceptorSkipAuthHeader);

    return this.http.post('login', params.toString(), {headers})
      .pipe(
        map((response: any) => new User(response)),
        tap((user: User) => this.storeUser(user)),
      );
  }

  refreshToken(): Observable<string> {
    const headers: HttpHeaders = new HttpHeaders()
      .set(InterceptorSkipAuthHeader, InterceptorSkipAuthHeader)

    return this.http.post(`token/refresh`, {refreshToken: this.getStoredTokens()?.refreshToken}, {headers})
      .pipe(
        map((response: any) => {
          const newToken = response.accessToken;
          this.updateAccessToken(newToken);

          return newToken;
        })
      )
  }

  logout(): void {
    localStorage.removeItem(USER_STORAGE_KEYS.user);
    this.router.navigate(['/login']);
  }

  storeUser(user: User): void {
    localStorage.setItem(USER_STORAGE_KEYS.user, JSON.stringify(user));
  }

  updateAccessToken(token: string): void {
    const storedUser = this.getStoredUser();
    if (storedUser) {
      storedUser.setAccessToken(token);
      this.storeUser(storedUser);
    }
  }

  getStoredUser(): User | null {
    const storageUser = localStorage.getItem(USER_STORAGE_KEYS.user);

    if (storageUser) {
      return new User(JSON.parse(storageUser));
    }

    return null;
  }

  getStoredTokens(): Tokens | null {
    const storageUser = localStorage.getItem(USER_STORAGE_KEYS.user);

    if (storageUser) {
      const user = new User(JSON.parse(storageUser));
      return user.tokens;
    }

    return null;
  }

  isRefreshTokenExpired(): boolean {
    const storedTokens = this.getStoredTokens()

    if (storedTokens) {
      const expireTime = new Date(storedTokens.expiresAt).getTime()
      return expireTime ? expireTime < new Date().getTime() : false;
    }

    return true
  }
}
