import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Paragraph} from "../../models/paragraph.model";
import {map, Observable} from "rxjs";
import {PageApi} from "../../models/page-api.model";

@Injectable({
  providedIn: 'root'
})
export class ParagraphApiService {

  constructor(private http: HttpClient) {
  }

  getParagraphByPage(page: number = 0): Observable<PageApi<Paragraph>> {
    return this.http.get(`paragraph?page=${page}`)
      .pipe(map((response: any) => new PageApi<Paragraph>(response, Paragraph)))
  }
}
