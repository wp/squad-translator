import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {LanguageService} from "../language.service";

@Injectable({
  providedIn: 'root'
})
export class ValidationApiService {

  constructor(private http: HttpClient, private languageService: LanguageService) {
  }

  validateText(textCorrectionId: number): Observable<any> {
    const languageId = this.languageService.getSelectedLanguage()?.id;

    return this.http.post('/validate', {textCorrectionId, languageId})
  }
}
