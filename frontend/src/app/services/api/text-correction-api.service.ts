import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TextCorrection} from "../../models/text-correction.model";
import {LanguageService} from "../language.service";

@Injectable({
  providedIn: 'root'
})
export class TextCorrectionApiService {

  constructor(private http: HttpClient, private languageService: LanguageService) {
  }

  createTextCorrection(textCorrectionId: string, text: string): Observable<TextCorrection> {
    const languageId = this.languageService.getSelectedLanguage()?.id;

    return this.http.post('/text-correction', {textCorrectionId, languageId, text})
      .pipe(map((response: any) => new TextCorrection(response)))
  }
}
