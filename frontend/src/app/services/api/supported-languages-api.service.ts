import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Language} from "../../models/language.model";

@Injectable({
  providedIn: 'root'
})
export class SupportedLanguagesApiService {

  constructor(private http: HttpClient) {
  }

  getLanguages(): Observable<Language[]> {
    return this.http.get('/supported-languages')
      .pipe(map((response: any) => response.map((item: string) => new Language(item))))
  }
}
