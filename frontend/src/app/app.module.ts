import {InjectionToken, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {NZ_I18N} from 'ng-zorro-antd/i18n';
import {en_US} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {environment} from "../environments/environment";
import {SquadHttpInterceptor} from "./core/http/squad-http.interceptor";

registerLocaleData(en);

export const BASE_API_URL = new InjectionToken<string>('Base Api Url', {
  providedIn: 'root',
  factory: () => ''
});


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: en_US},
    {provide: BASE_API_URL, useValue: environment.apiBaseUrl},
    {provide: HTTP_INTERCEPTORS, useClass: SquadHttpInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
