import {User} from "./user.model";

export class Validation {
  id: number;
  language: string;
  user: User;

  constructor(params: any) {
    this.id = params.id;
    this.language = params.language
    this.user = new User(params.user);
  }
}
