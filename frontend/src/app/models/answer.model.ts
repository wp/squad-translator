import {TextCorrection} from "./text-correction.model";

export class Answer {
  id: number;
  startFrom: number;
  text: string;
  textCorrections: TextCorrection[];

  constructor(params: any) {
    this.id = params.id;
    this.startFrom = params.start;
    this.text = params.text;
    this.textCorrections = params.textCorrections.map((correction: any) => new TextCorrection(correction))
  }
}
