export class PageApi<T> {
  content: T[];
  last: boolean;
  totalPages: number;
  totalElements: number;
  first: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  empty: boolean;

  constructor(params: any, classRef: any) {
    this.content = params.content.map((item: any) => new classRef(item));
    this.last = params.last;
    this.totalPages = params.totalPages;
    this.totalElements = params.totalElements;
    this.first = params.first;
    this.number = params.number + 1;
    this.numberOfElements = params.numberOfElements;
    this.size = params.size;
    this.empty = params.empty;
  }
}
