export class Language {
  isoCode: string;
  id: number;

  constructor(params: any) {
    this.id = params.id;
    this.isoCode = params.isoCode;
  }
}
