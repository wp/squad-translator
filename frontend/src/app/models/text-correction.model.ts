import {Validation} from "./validation.model";
import {Language} from "./language.model";

export class TextCorrection {
  id: number;
  textId: string;
  user: any;
  text: string;
  language: Language;
  validation: Validation | null;

  constructor(params: any) {
    this.id = params.id;
    this.textId = params.textId;
    this.user = params.user;
    this.text = params.text;
    this.language = params.language
    this.validation = params.validation && new Validation(params.validation);
  }
}
