export class Tokens {
  accessToken: string;
  refreshToken: string;
  expiresAt: Date;

  constructor(params: any) {
    this.accessToken = params.accessToken;
    this.refreshToken = params.refreshToken;
    this.expiresAt = new Date(params.expiresAt);
  }

  setAccessToken(token: string) {
    this.accessToken = token;
  }
}
