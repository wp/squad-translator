import {Question} from "./question.model";
import {TextCorrection} from "./text-correction.model";

export class Paragraph {
  id: number;
  text: string;
  questions: Question[];
  textCorrections: TextCorrection[];

  constructor(params: any) {
    this.id = params.id;
    this.text = params.context;
    this.questions = params.questions.map((question: any) => new Question(question))
    this.textCorrections = params.textCorrections.map((correction: any) => new TextCorrection(correction))
  }
}
