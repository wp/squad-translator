import {Answer} from "./answer.model";
import {TextCorrection} from "./text-correction.model";

export class Question {
  id: string;
  isImpossible: boolean;
  text: string;
  answers: Answer[];
  textCorrections: TextCorrection[];

  constructor(params: any) {
    this.id = params.id;
    this.isImpossible = params.isImpossible;
    this.text = params['question'];
    this.answers = params.answers.map((answer: any) => new Answer(answer))
    this.textCorrections = params.textCorrections.map((correction: any) => new TextCorrection(correction))
  }
}
