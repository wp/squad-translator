import {Tokens} from "./tokens.model";

export enum UserRoles {
  ADMIN,
  USER
}

export class User {
  name: string;
  username: string;
  email: string;
  role: UserRoles;
  tokens: Tokens;

  constructor(params: any) {
    this.name = params.name;
    this.username = params.username;
    this.email = params.email;
    this.role = params.role;
    this.tokens = params.tokens && new Tokens(params.tokens);
  }

  setAccessToken(token: string) {
    this.tokens.setAccessToken(token);
  }
}
