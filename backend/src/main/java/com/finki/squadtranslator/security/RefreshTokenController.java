package com.finki.squadtranslator.security;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.appuser.AppUserService;
import com.finki.squadtranslator.utils.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.finki.squadtranslator.utils.TokenUtils.REFRESH_TOKEN_EXPIRY_TIME;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping(path = "api/v1/token/refresh")
@AllArgsConstructor
public class RefreshTokenController {

    private AppUserService appUserService;

    @PostMapping
    public void refreshToken(@RequestBody JsonNode reqBody, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String refreshToken = reqBody.get("refreshToken").asText();
        if (refreshToken != null) {
            try {
                String username = TokenUtils.decodeToken(refreshToken);
                AppUser user = appUserService.loadUserByUsername(username);
                String token = TokenUtils.generateToken(user, request.getRequestURL().toString(), new Date(System.currentTimeMillis() + REFRESH_TOKEN_EXPIRY_TIME));

                Map<String, String> tokens = new HashMap<>();
                tokens.put("accessToken", token);
                response.setContentType(APPLICATION_JSON_VALUE);

                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception exception) {
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            response.setStatus(BAD_REQUEST.value());
            Map<String, String> error = new HashMap<>();
            error.put("error_message", "Refresh token is missing");
            response.setContentType(APPLICATION_JSON_VALUE);
            new ObjectMapper().writeValue(response.getOutputStream(), error);
        }
    }
}
