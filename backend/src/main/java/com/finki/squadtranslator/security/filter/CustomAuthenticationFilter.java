package com.finki.squadtranslator.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.appuser.AppUserTokens;
import com.finki.squadtranslator.appuser.AuthenticatedUser;
import com.finki.squadtranslator.utils.TokenUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import static com.finki.squadtranslator.utils.TokenUtils.REFRESH_TOKEN_EXPIRY_TIME;
import static com.finki.squadtranslator.utils.TokenUtils.TOKEN_EXPIRY_TIME;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);

        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        AppUser user = (AppUser) authResult.getPrincipal();

        Date refreshExpiresAt = new Date(System.currentTimeMillis() + REFRESH_TOKEN_EXPIRY_TIME);
        String token = TokenUtils.generateToken(user, request.getRequestURL().toString(), new Date(System.currentTimeMillis() + TOKEN_EXPIRY_TIME));
        String refreshToken = TokenUtils.generateToken(user, request.getRequestURL().toString(), refreshExpiresAt);
        AppUserTokens userTokens = new AppUserTokens(token, refreshToken, refreshExpiresAt);

        AuthenticatedUser authenticatedUser = new AuthenticatedUser(user.getName(), user.getUsername(), user.getEmail(), user.getAppUserRole(), userTokens);

        response.setContentType(APPLICATION_JSON_VALUE);

        new ObjectMapper().writeValue(response.getOutputStream(), authenticatedUser);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        Map<String, String> error = new HashMap<>();
        error.put("error_message", failed.getMessage());

        response.setStatus(BAD_REQUEST.value());
        response.setContentType(APPLICATION_JSON_VALUE);

        new ObjectMapper().writeValue(response.getOutputStream(), error);
    }
}
