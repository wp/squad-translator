package com.finki.squadtranslator.answer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.finki.squadtranslator.question.Question;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Answer {
    @Id
    private String id;
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    private Question question;
    private Long start;
    private String text;

    @Transient
    List<TextCorrection> textCorrections = new ArrayList<>();

    public Answer(String id, Question question, Long start, String text) {
        this.id = id;
        this.question = question;
        this.start = start;
        this.text = text;
    }
}
