package com.finki.squadtranslator.registration;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.appuser.AppUserRole;
import com.finki.squadtranslator.appuser.AppUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationService {

    private final AppUserService appUserService;
    private EmailValidator emailValidator;

    public String register(RegistrationRequest request) {
        boolean isValidEmail = emailValidator.test(request.getEmail());

        if (!isValidEmail) {
            throw new IllegalStateException("Email is not valid");
        }

        return appUserService.signUpUser(new AppUser(
                request.getFirstName(),
                request.getUsername(),
                request.getEmail(),
                request.getPassword(),
                AppUserRole.USER
        ));
    }
}
