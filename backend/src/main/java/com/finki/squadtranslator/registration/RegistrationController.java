package com.finki.squadtranslator.registration;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(path = "api/v1/register")
@AllArgsConstructor
public class RegistrationController {

    private RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<?> register(@RequestBody RegistrationRequest request) {
        try {
            registrationService.register(request);
            return new ResponseEntity<String>("User created", HttpStatus.OK);
        } catch (Exception exception) {
            Map<String, String> error = new HashMap<>();
            error.put("error_message", exception.getMessage());
            return new ResponseEntity<Map<String, String>>(error, HttpStatus.BAD_REQUEST);
        }
    }
}
