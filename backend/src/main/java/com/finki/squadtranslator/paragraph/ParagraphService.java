package com.finki.squadtranslator.paragraph;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import com.finki.squadtranslator.textCorrection.TextCorrectionRepository;
import com.finki.squadtranslator.validation.Validation;
import com.finki.squadtranslator.validation.ValidationRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class ParagraphService {
    private final ParagraphRepository paragraphRepository;
    private final TextCorrectionRepository textCorrectionRepository;
    private final ValidationRepository validationRepository;

    public Paragraph save(Paragraph paragraph) {
        return paragraphRepository.save(paragraph);
    }

    public void saveAll(List<Paragraph> paragraphs) {
        paragraphRepository.saveAll(paragraphs);
    }

    public List<Paragraph> getAllParagraphsByContext(List<String> contexts) {
        return paragraphRepository.findAllByContextIn(contexts);
    }

    public Page<Paragraph> getParagraphs(Integer page) {
        Page<Paragraph> paragraphs = paragraphRepository.findAll(PageRequest.of(page, 1));
        paragraphs.forEach(paragraph -> {
            List<String> textCorrectionIds = new ArrayList<>();
            textCorrectionIds.add("p:" + paragraph.getId());

            paragraph.getQuestions().forEach(question -> {
                textCorrectionIds.add("q:" + question.getId());

                question.getAnswers().forEach(answer -> {
                    textCorrectionIds.add("a:" + answer.getId());
                });
            });

            List<TextCorrection> textCorrections = textCorrectionRepository.findAllByTextIdIn(textCorrectionIds);

            paragraph.setTextCorrections(textCorrections.stream().filter(textCorrection -> Objects.equals(textCorrection.getTextId(), "p:" + paragraph.getId())).collect(Collectors.toList()));

            paragraph.getQuestions().forEach(question -> {
                question.setTextCorrections(textCorrections.stream().filter(textCorrection -> Objects.equals(textCorrection.getTextId(), "q:" + question.getId())).collect(Collectors.toList()));

                question.getAnswers().forEach(answer -> {
                    answer.setTextCorrections(textCorrections.stream().filter(textCorrection -> Objects.equals(textCorrection.getTextId(), "a:" + answer.getId())).collect(Collectors.toList()));
                });
            });
        });

        return paragraphs;
    }
}
