package com.finki.squadtranslator.paragraph;

import com.finki.squadtranslator.question.Question;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Paragraph {
    @Id
    private String id;

    @Column(columnDefinition = "TEXT")
    private String context;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "paragraph")
    private List<Question> questions = new ArrayList<>();

    @Transient
    private List<TextCorrection> textCorrections = new ArrayList<>();

    public Paragraph(String id, String context) {
        this.id = id;
        this.context = context;
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }
}
