package com.finki.squadtranslator.paragraph;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ParagraphRepository extends JpaRepository<Paragraph, Long> {
    List<Paragraph> findAllByContextIn(Collection<String> contexts);

}
