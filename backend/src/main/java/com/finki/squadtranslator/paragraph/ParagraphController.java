package com.finki.squadtranslator.paragraph;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "api/v1/paragraph")
@AllArgsConstructor
public class ParagraphController {

    private final ParagraphService paragraphService;

    @GetMapping
    public Page<Paragraph> getParagraphs(@RequestParam Integer page) {
        return paragraphService.getParagraphs(page);
    }
}
