package com.finki.squadtranslator.textCorrection;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.appuser.AppUserService;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguageService;
import com.finki.squadtranslator.utils.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "api/v1/text-correction")
@AllArgsConstructor
public class TextCorrectionController {

    private AppUserService appUserService;
    private TextCorrectionService textCorrectionService;

    private SupportedLanguageService supportedLanguageService;

    @PostMapping
    public ResponseEntity<?> correct(HttpServletRequest request, HttpServletResponse response, @RequestBody TextCorrectionRequest body) {
        try {
            String token = request.getHeader("Authorization").substring("Bearer ".length());
            String username = TokenUtils.decodeToken(token);
            AppUser user = appUserService.loadUserByUsername(username);

            SupportedLanguage supportedLanguage = supportedLanguageService.getById(body.getLanguageId());
            TextCorrection textCorrection = new TextCorrection(body.getTextCorrectionId(), user, body.getText(), supportedLanguage);
            textCorrectionService.save(textCorrection);

            return new ResponseEntity<TextCorrection>(textCorrection, HttpStatus.OK);
        } catch (Exception exception) {
            Map<String, String> error = new HashMap<>();
            error.put("error_message", exception.getMessage());
            return new ResponseEntity<Map<String, String>>(error, HttpStatus.BAD_REQUEST);
        }
    }
}
