package com.finki.squadtranslator.textCorrection;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.validation.Validation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class TextCorrection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String textId;

    @ManyToOne(fetch = FetchType.EAGER)
    public AppUser user;
    @Column(columnDefinition = "TEXT")
    private String text;

    @OneToOne(cascade = CascadeType.ALL)
    private Validation validation;

    @ManyToOne(fetch = FetchType.EAGER)
    private SupportedLanguage language;

    public TextCorrection(AppUser user, String text, SupportedLanguage language) {
        this.user = user;
        this.text = text;
        this.language = language;
    }

    public TextCorrection(String textId, AppUser user, String text, SupportedLanguage language) {
        this.textId = textId;
        this.user = user;
        this.text = text;
        this.language = language;
    }
}
