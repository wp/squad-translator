package com.finki.squadtranslator.textCorrection;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class TextCorrectionService {

    TextCorrectionRepository textCorrectionRepository;

    public TextCorrection save(TextCorrection textCorrection) {
        return textCorrectionRepository.save(textCorrection);
    }

    public void saveAll(List<TextCorrection> textCorrections) {
        textCorrectionRepository.saveAll(textCorrections);
    }

    public List<TextCorrection> findAll() {
        return textCorrectionRepository.findAll();
    }

    public TextCorrection findById(Long id) {
        return textCorrectionRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Text correction not found"));
    }

    public List<TextCorrection> findAllByTextId(Collection<String> textId) {
        return textCorrectionRepository.findAllByTextIdIn(textId);
    }

    public TextCorrection findByText(String text) {
        return textCorrectionRepository.findByText(text);
    }
}
