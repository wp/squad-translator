package com.finki.squadtranslator.textCorrection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class TextCorrectionRequest {
    private String textCorrectionId;
    private Long languageId;
    private String text;
}
