package com.finki.squadtranslator.textCorrection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TextCorrectionRepository extends JpaRepository<TextCorrection, Long> {

    List<TextCorrection> findAllByTextIdIn(Collection<String> ids);
    TextCorrection findByText(String text);
}
