package com.finki.squadtranslator.supportedLanguage;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SupportedLanguageService {

    SupportedLanguageRepository supportedLanguageRepository;

    public SupportedLanguage save(SupportedLanguage supportedLanguage) {
        return supportedLanguageRepository.save(supportedLanguage);
    }

    public List<SupportedLanguage> getAllLanguages() {
        return supportedLanguageRepository.findAll();
    }

    public SupportedLanguage getById(Long id) {
        return supportedLanguageRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Language not found"));
    }

    public Optional<SupportedLanguage> getByIsoCode(String code) {
        return supportedLanguageRepository.findSupportedLanguageByIsoCode(code);
    }
}
