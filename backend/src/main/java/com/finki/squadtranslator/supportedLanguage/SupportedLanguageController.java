package com.finki.squadtranslator.supportedLanguage;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping(path = "api/v1/supported-languages")
@AllArgsConstructor
public class SupportedLanguageController {

    SupportedLanguageService supportedLanguageService;

    @GetMapping
    public List<SupportedLanguage> getLanguages(HttpServletRequest request) {
        return supportedLanguageService.getAllLanguages();
    }
}
