package com.finki.squadtranslator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finki.squadtranslator.answer.Answer;
import com.finki.squadtranslator.answer.AnswerService;
import com.finki.squadtranslator.paragraph.Paragraph;
import com.finki.squadtranslator.paragraph.ParagraphService;
import com.finki.squadtranslator.question.Question;
import com.finki.squadtranslator.question.QuestionService;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import com.finki.squadtranslator.textCorrection.TextCorrectionService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class JsonParser {
    public void importTextCorrection(TextCorrectionService textCorrectionService, String id, String text) {
        TextCorrection questionCorrection = new TextCorrection(id, null, text, new SupportedLanguage(1L, "mk"));
        textCorrectionService.save(questionCorrection);
    }

    //    TODO: Uncomment this when you want to import new language data
//    @Bean
//    CommandLineRunner parseJson(ParagraphService paragraphService, QuestionService questionService, AnswerService answerService, TextCorrectionService textCorrectionService) {
//        return args -> {
//            File jsonFile = new File("src/main/resources/json/train-mk.json").getAbsoluteFile();
//
//            JsonNode productNode = new ObjectMapper().readTree(jsonFile);
//            productNode.get("data").forEach(item -> {
//                item.get("paragraphs").forEach(paragraph -> {
//                    Paragraph paragraphForSave = new Paragraph(paragraph.get("context").textValue());
//                    Paragraph savedParagraph = paragraphService.save(paragraphForSave);
//                    importTextCorrection(textCorrectionService, "p:" + savedParagraph.getId(), paragraph.get("context_mk").get("google_ctx").textValue());
//
//                    paragraph.get("qas").forEach(question -> {
//                        Question question1 = new Question(question.get("id").asText(), paragraphForSave, question.get("is_impossible").asBoolean(), question.get("question").asText());
//                        Question savedQuestion = questionService.save(question1);
//                        importTextCorrection(textCorrectionService, "q:" + savedQuestion.getId(), question.get("question_mk").asText());
//
//                        question.get("answers").forEach(answer -> {
//                            Answer answer1 = new Answer(question1, answer.get("answer_start").asLong(), answer.get("text").asText());
//                            Answer savedAnswer = answerService.save(answer1);
//                            if (answer.get("text_mk") != null && answer.get("text_mk").asText().length() > 0) {
//                                importTextCorrection(textCorrectionService, "a:" + savedAnswer.getId(), answer.get("text_mk").asText());
//                            }
//                        });
//                    });
//                });
//            });
//        };
//    }
}