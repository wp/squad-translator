package com.finki.squadtranslator.upload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finki.squadtranslator.answer.Answer;
import com.finki.squadtranslator.answer.AnswerService;
import com.finki.squadtranslator.paragraph.Paragraph;
import com.finki.squadtranslator.paragraph.ParagraphService;
import com.finki.squadtranslator.question.Question;
import com.finki.squadtranslator.question.QuestionService;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguageService;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import com.finki.squadtranslator.textCorrection.TextCorrectionService;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class UploadController {

    SupportedLanguageService supportedLanguageService;
    ParagraphService paragraphService;
    TextCorrectionService textCorrectionService;
    QuestionService questionService;
    AnswerService answerService;

    @PostMapping(path = "api/v1/create-paragraphs")
    public ResponseEntity<?> createParagraphs(@RequestPart("file") @NotNull MultipartFile file) throws IOException {
        JsonNode productNode = new ObjectMapper().readTree(file.getInputStream());

        List<Paragraph> paragraphs = new ArrayList<>();

        productNode.get("data").forEach(item -> {
            item.get("paragraphs").forEach(paragraph -> {
                Paragraph paragraphForSave = new Paragraph(paragraph.get("id").textValue(), paragraph.get("context").textValue());
                paragraphs.add(paragraphForSave);

                paragraph.get("qas").forEach(question -> {
                    Question questionForSave = new Question(question.get("id").asText(), paragraphForSave, question.get("is_impossible").asBoolean(), question.get("question").asText());
                    paragraphForSave.addQuestion(questionForSave);

                    question.get("answers").forEach(answer -> {
                        Answer answerForSave = new Answer(answer.get("id").asText(), questionForSave, answer.get("answer_start").asLong(), answer.get("text").asText());
                        questionForSave.addAnswer(answerForSave);
                    });
                });
            });
        });

        paragraphService.saveAll(paragraphs);
        return new ResponseEntity<>("Successfully uploaded!", HttpStatus.OK);
    }

    @PostMapping(path = "api/v1/insert-translation")
    public ResponseEntity<?> uploadJson(@RequestParam("language") String language, @RequestPart("file") @NotNull MultipartFile file) throws IOException {
        Optional<SupportedLanguage> supportedLanguage = supportedLanguageService.getByIsoCode(language);
        JsonNode productNode = new ObjectMapper().readTree(file.getInputStream());

        if (supportedLanguage.isPresent()) {
            return new ResponseEntity<>("Language exist", HttpStatus.BAD_REQUEST);
        } else {
            SupportedLanguage savedLang = supportedLanguageService.save(new SupportedLanguage(language));

            List<TextCorrection> paragraphTextCorrections = new ArrayList<>();
            List<TextCorrection> questionTextCorrections = new ArrayList<>();
            List<TextCorrection> answerTextCorrections = new ArrayList<>();

            productNode.get("data").forEach(item -> {
                item.get("paragraphs").forEach(paragraph -> {
                    paragraphTextCorrections.add(new TextCorrection("p:" + paragraph.get("id").asText(), null, paragraph.get("context_" + language).get("google_ctx").textValue(), savedLang));

                    paragraph.get("qas").forEach(question -> {
                        questionTextCorrections.add(new TextCorrection("q:" + question.get("id").asText(), null, question.get("question_" + language).asText(), savedLang));

                        question.get("answers").forEach(answer -> {
                            if (answer.get("answer_" + language) != null) {
                                answerTextCorrections.add(new TextCorrection("a:" + answer.get("id").asText(), null, answer.get("answer_" + language).asText(), savedLang));
                            }
                        });
                    });
                });
            });

            textCorrectionService.saveAll(paragraphTextCorrections);
            textCorrectionService.saveAll(questionTextCorrections);
            textCorrectionService.saveAll(answerTextCorrections);

            return new ResponseEntity<>("Successfully uploaded!", HttpStatus.OK);
        }
    }
}
