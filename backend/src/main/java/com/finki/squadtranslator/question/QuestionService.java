package com.finki.squadtranslator.question;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {
    QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public Question save(Question question) {
        return questionRepository.save(question);
    }

    public void saveAll(List<Question> questions) {
        questionRepository.saveAll(questions);
    }
}
