package com.finki.squadtranslator.question;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.finki.squadtranslator.answer.Answer;
import com.finki.squadtranslator.paragraph.Paragraph;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Question {
    @Id
    private String id;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Paragraph paragraph;
    private Boolean isImpossible;
    @Column(columnDefinition = "TEXT")
    private String question;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "question")
    private List<Answer> answers = new ArrayList<>();

    @Transient
    List<TextCorrection> textCorrections = new ArrayList<>();

    public Question(String id, Paragraph paragraph, Boolean isImpossible, String question) {
        this.id = id;
        this.paragraph = paragraph;
        this.isImpossible = isImpossible;
        this.question = question;
    }

    public void addAnswer(Answer answer) {
        this.answers.add(answer);
    }
}
