package com.finki.squadtranslator.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ValidationRequest {
    private Long textCorrectionId;
    private Long languageId;
}
