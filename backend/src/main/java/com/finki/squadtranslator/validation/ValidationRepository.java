package com.finki.squadtranslator.validation;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ValidationRepository extends JpaRepository<Validation, Long> {

    List<Validation> findAllByUserAndTextCorrectionIn(AppUser appUser, Collection<TextCorrection> textCorrections);

}
