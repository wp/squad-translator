package com.finki.squadtranslator.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Validation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToOne(mappedBy = "validation", cascade = CascadeType.ALL)
    public TextCorrection textCorrection;

    @ManyToOne(fetch = FetchType.LAZY)
    public AppUser user;

    @ManyToOne(fetch = FetchType.EAGER)
    private SupportedLanguage language;

    private Long textCorrectionId;

    public Validation(AppUser user, SupportedLanguage language, Long textCorrectionId) {
        this.user = user;
        this.language = language;
        this.textCorrectionId = textCorrectionId;
    }
}
