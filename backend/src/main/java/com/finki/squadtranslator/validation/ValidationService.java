package com.finki.squadtranslator.validation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ValidationService {

    ValidationRepository validationRepository;

    public Validation save(Validation validation) {
        return validationRepository.save(validation);
    }
}
