package com.finki.squadtranslator.validation;

import com.finki.squadtranslator.appuser.AppUser;
import com.finki.squadtranslator.appuser.AppUserService;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguage;
import com.finki.squadtranslator.supportedLanguage.SupportedLanguageService;
import com.finki.squadtranslator.textCorrection.TextCorrection;
import com.finki.squadtranslator.textCorrection.TextCorrectionService;
import com.finki.squadtranslator.utils.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "api/v1/validate")
@AllArgsConstructor
public class ValidationController {

    private ValidationService validationService;
    private AppUserService appUserService;
    private TextCorrectionService textCorrectionService;
    private SupportedLanguageService supportedLanguageService;

    @PostMapping
    public ResponseEntity<?> validate(HttpServletRequest request, HttpServletResponse response, @RequestBody ValidationRequest body) {
        try {
            String token = request.getHeader("Authorization").substring("Bearer ".length());
            String username = TokenUtils.decodeToken(token);
            AppUser user = appUserService.loadUserByUsername(username);
            SupportedLanguage supportedLanguage = supportedLanguageService.getById(body.getLanguageId());
            Validation validation = new Validation(user, supportedLanguage, body.getTextCorrectionId());
            validationService.save(validation);

            TextCorrection textCorrection = textCorrectionService.findById(body.getTextCorrectionId());
            List<TextCorrection> textCorrections = textCorrectionService.findAllByTextId(List.of(textCorrection.getTextId()));
            textCorrections.forEach(textCorrection1 -> {
                if (textCorrection1.getValidation() != null) {
                    textCorrection1.setValidation(null);
                    textCorrectionService.save(textCorrection1);
                }
            });
            textCorrection.setValidation(validation);
            textCorrection.setUser(validation.getUser());
            textCorrectionService.save(textCorrection);

            return new ResponseEntity<TextCorrection>(textCorrection, HttpStatus.OK);
        } catch (Exception exception) {
            Map<String, String> error = new HashMap<>();
            error.put("error_message", exception.getMessage());
            return new ResponseEntity<Map<String, String>>(error, HttpStatus.BAD_REQUEST);
        }
    }
}