package com.finki.squadtranslator.appuser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AuthenticatedUser {

    private String name;
    private String username;
    private String email;
    private AppUserRole role;
    private AppUserTokens tokens;
}
