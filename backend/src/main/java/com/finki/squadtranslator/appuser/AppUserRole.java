package com.finki.squadtranslator.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
