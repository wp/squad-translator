package com.finki.squadtranslator.appuser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class AppUserTokens {
    private String accessToken;
    private String refreshToken;
    private Date expiresAt;
}
