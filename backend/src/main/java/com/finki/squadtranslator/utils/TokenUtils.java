package com.finki.squadtranslator.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.finki.squadtranslator.appuser.AppUser;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;
import java.util.stream.Collectors;

public final class TokenUtils {

    private static final Algorithm algorithm = Algorithm.HMAC256("8wmC4NR5HfrOQVDI9rVUZKATMyJ2lkiy".getBytes());
    public final static int TOKEN_EXPIRY_TIME = 720 * 60 * 1000;
    public final static int REFRESH_TOKEN_EXPIRY_TIME = 1440 * 60 * 1000;

    public static String decodeToken(String token) {
        Algorithm algorithm = TokenUtils.algorithm;
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);

        return decodedJWT.getSubject();
    }

    public static String generateToken(AppUser user, String issuer, Date expireDate) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(expireDate)
                .withIssuer(issuer)
                .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(TokenUtils.algorithm);
    }
}
