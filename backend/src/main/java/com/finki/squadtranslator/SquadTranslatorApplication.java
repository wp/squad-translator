package com.finki.squadtranslator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SquadTranslatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SquadTranslatorApplication.class, args);
    }
}
