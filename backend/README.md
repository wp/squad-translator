# SQuAD Translator Backend

### How to first time run application:
1. Use Oracle OpenJDK version 18.x.x
2. Open the project and install dependencies
3. Prepare file with translations, there is one in `resources/json/train-mk.json`
4. If not dockerised, init local postgres database with info from `application.properties`
5. Start the application
6. Register first user with sending POST request to `api/v1/register`. Body of this request can be found in `RegistrationRequest.java`
7. Login with that user on frontend application (or Postman) and take the auth bearer token (from localhost)
8. Send POST request to `api/v1/create-paragraphs` as form-data where you will have one field with key: `file` and value:`train-mk.json` file. Bearer token should be used for auth on this call.
9. Send POST request to `api/v1/insert-translation` as form-data where you will have two fields. First will be with key: `language` and value ISO code of that language, for example `mk` and the second field will be with key: `file` and value:`train-mk.json` file. Bearer token should be used for auth on this call.
10. You have created user and database that can be used with frontend application.

Application can be dockerised with running first Dockerfile then docker-compose.yml
