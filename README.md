# SQuAD Translator

## Getting started

This repository have 2 projects inside:

- In `backend` directory there is Spring Boot backend application, for further info check `README.md` file inside that
  directory
- In `frontend` directory there is Angular frontend application, for further info check `README.md` file inside that
  directory 
